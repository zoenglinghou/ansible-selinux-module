# selinux-module

This Ansible role set SELinux policy via a type enforcement file. It optionally generates the enforcement file from audit log.

## Table of content

- [Default Variables](#default-variables)
  - [generate_te](#generate_te)
  - [program_name](#program_name)
  - [te_dir](#te_dir)
  - [te_name](#te_name)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### generate_te

If a type enforcement file is generated

#### Default value

```YAML
generate_te: false
```

### program_name

Binary name of the program of the module policy

#### Default value

```YAML
program_name:
```

### te_dir

Path to the local directory of the `.te` file

#### Default value

```YAML
te_dir:
```

### te_name

Name of the `.te` enforcement type file

#### Default value

```YAML
te_name:
```



## Dependencies

None.

## License

license (GPL-2.0-or-later, MIT, etc)

## Author

Linghao Zhang
